package;
 
import flixel.FlxG;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxVirtualPad;

/**
 * ...
 * @author .:BuzzJeux:.
 */
class PlayState extends FlxState
{
	var _level:TiledLevel;
	var _howto:FlxText;
	var speed:Float;
	var startTime:Float;
	var points:Int;
	
	#if mobile
	var _virtualPad:FlxVirtualPad;
	#end

	override public function create():Void
	{
		//FlxG.mouse.visible = false;
		//bgColor = 0xFF18A068;
		bgColor = 0xFFFFA068;
		
		// Load the level's tilemaps
		_level = new TiledLevel();
		
		// Add tilemaps
		add(_level.backgroundTiles);
		
		// Add tilemaps
		add(_level.playerTiles);
		
		startTime = 0;
		speed = 1;
		points = 0;
		
		#if !mobile
			// Set and create Txt Howto
			_howto = new FlxText(30, 20, FlxG.width);
			_howto.alignment = CENTER;
			_howto.text = "Current Speed = " + speed + "\n points = " + points;
			_howto.scrollFactor.set(0, 0);
			add(_howto);
		#end

		
		#if mobile
			_virtualPad = new FlxVirtualPad(FULL, NONE);
			_virtualPad.alpha = 0.5;
			FlxG.state.add(_virtualPad);
		#end
	}
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		startTime+=elapsed;

		if(FlxG.keys.anyJustReleased([LEFT, A])) {
			_level.moveLeft();
		} 
		if( FlxG.keys.anyJustReleased([RIGHT, D])) {
			_level.moveRight();
		} 
		if( FlxG.keys.anyJustReleased([UP, W])) {
			_level.rotate();
		} 
		if( FlxG.keys.anyJustReleased([DOWN, SPACE, S])) {
			_level.moveDown();
		} 

		if( FlxG.keys.anyJustReleased([M])) {
		_level.testclear();
		}
		if (startTime > speed)
		{
			_level.progressMovement();
			startTime = 0;
		}
		//trace(_level.rowscleared);
		if (_level.rowscleared > 0) {
			points += (_level.rowscleared * _level.rowscleared) * 10;
			_level.rowscleared = 0;
			
			#if !mobile
				_howto.text = "Current Speed = " + speed + "\n points = " + points;
			#end
		}
	}
	
	override public function destroy():Void
	{
		super.destroy();
		
		_level = null;
		_howto = null;
	}
}
