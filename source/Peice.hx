

enum Direction {
    Top;
    Left;
    Bottom;
    Right;
}
class Peice {
    var direction:Direction;

     var top:Array<Array<Int>>;
     var left:Array<Array<Int>>;
     var bottom:Array<Array<Int>>;
     var right:Array<Array<Int>>;

	public function new()
	{
        direction = Direction.Top;
	}
    public function rotateDirection() {
        if(direction==Direction.Top) {
            direction = Direction.Left;
        } else if (direction==Direction.Left) {
            direction = Direction.Bottom;
        } else if (direction==Direction.Bottom) {
            direction = Direction.Right;
        } else if (direction==Direction.Right) {
            direction = Direction.Top;
        }
    }

    public function currentPeice () {
        if(direction==Direction.Top) {
            return top;
        } else if (direction==Direction.Left) {
            return left;
        } else if (direction==Direction.Bottom) {
            return bottom;
        }
        return right;
    }
}
