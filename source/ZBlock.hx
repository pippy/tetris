
class ZBlock extends Peice {
	public function new()
	{
        super();
        top = [
            [0,0,0],
            [1,1,0],
            [0,1,1]];
        left = [
            [0,0,1],
            [0,1,1],
            [0,1,0]];
        bottom = [
            [0,0,0],
            [1,1,0],
            [0,1,1]];
        right = [
            [0,0,1],
            [0,1,1],
            [0,1,0]];
	}
}
