package;


import flixel.tile.FlxTilemap;


class TiledLevel 
{
	public var playerTiles:FlxTilemap;
	public var backgroundTiles:FlxTilemap;
	static var peice:Peice;

	var currentx:Int;
	var currenty:Int;
	public var rowscleared:Int;

	public function new()
	{
		rowscleared = 0;
		buildbackground();
		createplayerarray();
		resetpeice();
		pickrandomblock();
		renderplayerpeices();
	}

	public function resetpeice() {
		currentx = 4;
		currenty = 1;
	}

	public function pickrandomblock() {
		switch Std.random(7){
			case 0:
				peice = new IBlock();
			case 1:
				peice = new JBlock();
			case 2:
				peice = new LBlock();
			case 3:
				peice = new OBlock();
			case 4:
				peice = new SBlock();
			case 5:
				peice = new TBlock();
			case 6:
				peice = new ZBlock();
			
		}
	}

	// draw the moving bits to the screen
	public function renderplayerpeices() {
		for ( x in 0...backgroundTiles.widthInTiles) {
			for ( y in 0...backgroundTiles.heightInTiles) {
				playerTiles.setTile(x,y,0);
				
				var currentArray = peice.currentPeice();
				
				var deltax = x - currentx;
				var deltay = y - currenty;
				
				if(exists(currentArray,deltax,deltay) != 0) {
					playerTiles.setTile(x,y,2);
				}
			}
		}
	}

	// init the grid of blocks
	public function buildbackground () {
		var mapData:Array<Array<Int>> = [
		[1,1,1,1,1,1,1,1,1,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,0,0,0,0,0,0,0,0,1],
		[1,1,1,1,1,1,1,1,1,1]];

		backgroundTiles = new FlxTilemap();
		backgroundTiles.loadMapFrom2DArray(mapData, "assets/data/tiles.png", 16, 16);
	}

	// green player peices
	public function createplayerarray() {
		var playerData:Array<Array<Int>> = [
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0]];

		playerTiles = new FlxTilemap();
		playerTiles.loadMapFrom2DArray(playerData, "assets/data/tiles.png", 16, 16);
	}

	// given a matrix and an x and y position, does this item contain a block?
	public function exists (myarray:Array<Array<Int>>, x:Int, y:Int) {
		if (x < 0) {
			return 0;
		}
		if (y < 0) {
			return 0;
		}
		if (x >= myarray.length) {
			return 0;
		}
		if (y >= myarray[x].length) {
			return 0;
		}
		return myarray[x][y];
	}
	
	// move block down, figure shit out
	public function progressMovement () {
		if(testintersection(currentx,currenty+1)) {
			// render the peice to the board
			for ( x in 0...backgroundTiles.widthInTiles) {
				for ( y in 0...backgroundTiles.heightInTiles) {
					var currentArray = peice.currentPeice();
					
					if(exists(currentArray,x - currentx,y - currenty) != 0) {
						backgroundTiles.setTile(x,y,1);
					}
				}
			}
			checkforrows();
			resetpeice();		
			pickrandomblock();
			renderplayerpeices();
		}

		currenty++;
		renderplayerpeices();
	}

	// given a future state, will this position intersect with the map?
	public function testintersection(textx:Int, testy:Int) {
		for ( x in 0...backgroundTiles.widthInTiles) {
			for ( y in 0...backgroundTiles.heightInTiles) {
				var currentArray = peice.currentPeice();
				
				var deltax = x - textx;
				var deltay = y - testy;
				
				if(exists(currentArray,deltax,deltay) != 0) {
					if(backgroundTiles.getTile(x,y) != 0) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public function moveRight () {
		if(!testintersection(currentx+1,currenty)) {
			currentx++;
			renderplayerpeices();
		}
	}
	public function moveLeft () {
		if(!testintersection(currentx-1,currenty)) {
			currentx--;
			renderplayerpeices();
		}
	}
	public function moveDown() {
		var moveto = 0;
		for(i in 0...5) {
			if(testintersection(currentx,currenty+i)) {
				break;
			}
			moveto = i;
		}
		
		currenty+=moveto;
		renderplayerpeices();
	}

	public function rotate () {
		peice.rotateDirection();
		
		// if you've failed intersection, rotate back
		if(testintersection(currentx,currenty)) {
			peice.rotateDirection();
			peice.rotateDirection();
			peice.rotateDirection();
		}
		renderplayerpeices();
	}

	// check for points
	public function checkforrows() {
		for ( y in 1...backgroundTiles.heightInTiles-1) {
			var clear = true;
			for ( x in 1...backgroundTiles.widthInTiles-1) {
				if(backgroundTiles.getTile(x,y) == 0) {
					clear = false;
				}
			}
			// there's a row!
			if(clear) {
				rowscleared++;
				clearrow(y);
			}
		}
	}

	public function testclear() {
		clearrow(13);
	}

	// remove a row, and bring everything above it down
	public function clearrow(rownumber:Int) {
		// move everything down
		for ( yy in 1...backgroundTiles.heightInTiles) {
			var y = backgroundTiles.heightInTiles - yy;
			if (y>rownumber) {
				continue;
			}
			for ( xx in 1...backgroundTiles.widthInTiles-1) {
				var currenttile = backgroundTiles.getTile(xx,y-1);
				if(y == 1) {
					currenttile = 0;
				}
				backgroundTiles.setTile(xx,y,currenttile);
			}
			
		}
	}
}
