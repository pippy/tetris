
class LBlock extends Peice {
	public function new()
	{
        super();
        top = [
            [0,0,0],
            [1,1,1],
            [0,0,1]];
        left = [
            [0,1,0],
            [0,1,0],
            [1,1,0]];
        bottom = [
            [1,0,0],
            [1,1,1],
            [0,0,0]];
        right = [
            [0,1,1],
            [0,1,0],
            [0,1,0]];
	}
}
